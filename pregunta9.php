<?php
function findPasswordFromHash($targetHash)
{
    $charset = 'abcdefghijklmnopqrstuvwxyz0123456789';
    $charsetLength = strlen($charset);

    for ($i = 0; $i < $charsetLength; $i++) {
        for ($j = 0; $j < $charsetLength; $j++) {
            $password = $charset[$i] . $charset[$j];
            $hashedPassword = md5($password);
            if ($hashedPassword === $targetHash) {
                return $password;
            }
        }
    }

    return false;
}

$targetHash = '49f68a5c8493ec2c0bf489821c21fc3b';
$password = findPasswordFromHash($targetHash);
if ($password) {
    echo "La contraseña es: $password";
} else {
    echo "No se encontró la contraseña de 2 caracteres para la hash proporcionada.";
}

/* 
Explicación:
La función principal, findPasswordFromHash($targetHash), utiliza una hash MD5 como entrada y, si se encuentra, 
devuelve la contraseña correspondiente de dos caracteres. Devuelve falso si no se encuentra ninguna coincidencia.
Se crea una cadena $charset que contiene todos los caracteres alfanuméricos que se utilizarán para crear combinaciones de contraseñas. 
El bucle de generación de combinaciones utiliza dos bucles for anidados para crear todas las combinaciones posibles de dos caracteres 
utilizando los caracteres del código $charset.
La hash MD5 se calcula utilizando la función md5 para cada combinación de caracteres.
La comparación con el hash objetivo es la siguiente: La hash MD5 calculada y la hash MD5 objetivo se comparan. 
Se devuelve la contraseña correspondiente si hay una coincidencia.
La función se utiliza: $targetHash es la hash MD5 objetivo y se utiliza como argumento para la función findPasswordFromHash().
Dependiendo del resultado de la función, se muestra la contraseña encontrada o se muestra un mensaje que indica que no se encontró 
ninguna contraseña de dos caracteres para la hash proporcionada.
*/