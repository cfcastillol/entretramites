const timestamp = 1000;
const date = new Date(timestamp * 1000);
const formattedDate = date.toLocaleString("en-US", {
  timeZone: "Europe/Moscow",
});

console.log(formattedDate);

/* 
Explicación:
Define el timestamp UNIX deseado (1000 segundos).
Creo un objeto Date a partir del timestamp multiplicado por 1000 para convertirlo a milisegundos.
Utilizo el método toLocaleString() del objeto Date para formatear la fecha y hora.
Especifico la zona horaria deseada como 'Europe/Moscow'.
Imprimo la fecha y hora formateadas en la consola.
*/
