function lengthStringsArray(names) {
  if (!Array.isArray(names)) {
    return "El argumento debe ser un array";
  }

  return names.map((name) => {
    if (typeof name !== "string") {
      return "Todos los elementos del array deben ser strings";
    }
    return name.length;
  });
}

const names = ["cristian", "Fabian", "Katherine"];
const lengths = lengthStringsArray(names);
console.log(lengths);

/* 
Explicación:
Declaración de la Función: Utilizo function lengthStringsArray(names) para declarar la función, 
lo que la hace disponible en todo el ámbito donde está definida.
Validación del Argumento: Compruebo si names es un array usando Array.isArray(names). Si no lo es, lanzo un error.
Dentro del map, verifico si cada elemento es un string antes de obtener su longitud. Si no es un string, lanzo un error.
*/
