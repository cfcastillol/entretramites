<?php

function obtenerPrimos($n)
{
    if ($n <= 0) {
        return [];
    }

    $primos = [];
    $numero = 2;

    while (count($primos) < $n) {
        $esPrimo = true;
        foreach ($primos as $p) {
            if ($numero % $p == 0) {
                $esPrimo = false;
                break;
            }
            if ($p * $p > $numero) {
                break;
            }
        }
        if ($esPrimo) {
            $primos[] = $numero;
        }
        $numero++;
    }

    return $primos;
}

$n = 10;
$primerosPrimos = obtenerPrimos($n);
print_r($primerosPrimos);
